unit �reateFormLoadProfile;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  NameLoadProfile, Vcl.StdCtrls, TimeIntervalFrame, EnergyValueFrame,
  LoadProfile, EnergyFractionFrame;

type
  TForm2 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Frame21: TFrame2;
    Frame31: TFrame3;
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Frame11Edit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    buferLoadProfile: TloadProfile;
    procedure SetFirstStep;
    procedure DefaultTimeIntervalSettings;
    function CheckData(buferLoadProfile: TloadProfile): boolean;
    procedure PrepareLastStep;
    procedure ClearSeries;
    procedure PlotDefaultState;
    procedure EnergyFractionCalc;
    procedure TimeListInit;
    procedure ComboxFirstActivate;
    procedure SaveTimeIntervals;
    procedure CheckButtonEnable;

    { Private declarations }
  public
    procedure TransferName(inputLoadProfileName: string);

    { Public declarations }
  end;

const
  stepsCount: byte = 2;
  timeIntervals:array[0..6] of byte = (1,2,3,4,6,12,24);

var
  Form2: TForm2;
  currentStep: 1..3;
  LoadProfileName: string;

implementation

{$R *.dfm}

uses
  MenuLoadProfiles;

procedure HideAllSteps;
begin
  Form2.Frame31.Visible := false;
  Form2.Frame21.Visible := false;
end;

procedure ShowCurrent;
begin

  HideAllSteps;

  case currentStep of

    1:
      Form2.Frame21.Visible := true;

    2:
      Form2.Frame31.Visible := true;

  end;

end;

procedure TForm2.Button1Click(Sender: TObject);
begin




  currentStep := currentStep - 1;
  ShowCurrent;

  CheckButtonEnable();


end;

procedure TForm2.CheckButtonEnable;
begin

Button1.Enabled:=true;
Button2.Enabled:=true;

case currentStep of

1:Button1.Enabled :=false;

end;


end;

function TForm2.CheckData(buferLoadProfile: TloadProfile): boolean;
begin

  result := true;

end;

procedure TForm2.ClearSeries;
begin

  Frame31.Chart1.SeriesList[0].Clear;
  Frame31.Chart1.SeriesList[1].Clear;

end;

procedure TForm2.Button2Click(Sender: TObject);
begin

  currentStep := currentStep + 1;
  ShowCurrent;

  if currentStep = 2 then
  begin

    SaveTimeIntervals();
    PrepareLastStep;

  end;

  if (currentStep = 3) and CheckData(buferLoadProfile) then
  begin

    Form3.StartSaveProcess(buferLoadProfile);

    close;

  end;

  CheckButtonEnable();

end;

procedure ClearBuferLoad();
begin

//  buferLoadProfile := Default(TloadProfile);

end;

procedure TForm2.FormActivate(Sender: TObject);
begin

  SetFirstStep;

  DefaultTimeIntervalSettings;

  ComboxFirstActivate;

  CheckButtonEnable();

//  ClearBuferLoad();

end;

procedure TForm2.FormCreate(Sender: TObject);
begin

TimeListInit;



end;

procedure TForm2.Frame11Edit1Change(Sender: TObject);
begin

Form2.Frame21.TimeIntervalStringgrid.RowCount := 5;

end;

procedure TForm2.PlotDefaultState;
var
  i: integer;
  xvalues:TListValues;
  yvalues:TListValues;
  pointCount:Byte;
begin

   xvalues:=buferLoadProfile.data.GetTimeValues();
   yvalues:=buferLoadProfile.data.GetEnergyValues();

  for i := 0 to Frame21.Chart1.SeriesList[0].Count - 1 do
    Frame31.Chart1.SeriesList[0].AddXY(Frame21.Chart1.SeriesList[0].XValue[i],
     Frame21.Chart1.SeriesList[0].YValue[i]);



  pointCount:=buferloadProfile.data.PointCount;


  for i := 0 to pointCount-1 do
   begin

   Frame31.Chart1.SeriesList[1].AddXY(xvalues[i],yvalues[i])


   end;




end;

procedure TForm2.PrepareLastStep;
begin

  EnergyFractionCalc();
  ClearSeries();
  PlotDefaultState();

end;

procedure TForm2.SaveTimeIntervals;
var
  i: Integer;
begin

buferLoadProfile.data.count:=strtoint(Frame21.ComboBox1.Text);

for i := 1 to Frame21.TimeIntervalStringgrid.RowCount-1 do
    buferLoadProfile.data.data[i-1].time:=
    strtofloat(Frame21.TimeIntervalStringgrid.Cells[1,i]);



end;

procedure TForm2.SetFirstStep;
begin
  currentStep := 1;
  ShowCurrent;
end;

procedure TForm2.DefaultTimeIntervalSettings;
begin


  Frame21.TimeIntervalStringgrid.RowCount:=strtoint(Frame21.ComboBox1.Text);
  Frame21.TimeIntervalStringgrid.Cells[0, 1] := '�������� 1';
  Frame21.TimeIntervalStringgrid.Cells[1, 0] := '��������';
  Frame21.TimeIntervalStringgrid.Cells[1, 1] := '1';
  Frame21.Chart1.SeriesList[0].Clear;
  Frame21.Chart1.SeriesList[0].AddXY(0, 0);
  Frame21.Chart1.SeriesList[0].AddXY(1, 0);
  Frame21.Chart1.AllowZoom := false;
  Frame31.Chart1.AllowZoom := false;

end;

procedure TForm2.EnergyFractionCalc;
begin

buferLoadProfile.data.SetEqualEnergyFractions;

end;

procedure TForm2.TransferName(inputLoadProfileName: string);
begin

//  LoadProfileName := inputLoadProfileName;

  buferLoadProfile.name := inputLoadProfileName;

end;

procedure TForm2.ComboxFirstActivate;
begin
  Frame21.ComboBox1.ItemIndex := 0;
  Frame21.ComboBox1.OnChange(Frame21.ComboBox1);
end;

procedure TForm2.TimeListInit;
var
  item: Byte;
begin
  for item in timeIntervals do
    Frame21.ComboBox1.Items.Add(item.tostring);
  Frame21.ComboBox1.itemindex := 0;
end;

end.


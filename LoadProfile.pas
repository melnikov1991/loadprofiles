unit LoadProfile;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  NameLoadProfile, Vcl.StdCtrls, TimeIntervalFrame, EnergyValueFrame;

type
  Titem = record
    time: Double;
    energy: Double;
  end;

  TListValues = array[0..49] of double;

  Tdataloadset = record
    data: array[0..23] of TItem;
    count: Byte;
  private
    fpointCount: Byte;
    function GetPointCount: Byte;
  public
    procedure SetEqualEnergyFractions;
    function GetTimeValues: TListValues;
    function GetEnergyValues: TListValues;
  public
    property PointCount: Byte read GetPointCount;
  end;

  TCurveTemplate = array[0..23] of Titem;

  TLoadprofile = record
    name: string[10];
    comment: ShortString;
    data: TdataloadSet;
    curveTemplate: TCurveTemplate
  end;

implementation

{ Tdataloadset }

function Tdataloadset.GetEnergyValues: TListValues;
var
  nValues: Integer;
  i, k: Integer;
  buferArr: array[0..23] of double;
  nresultValues: Integer;
begin

  nValues := self.count + 1;

  nresultValues := nValues + (nValues - 2);
  result[0] := self.data[0].energy;
  result[1] := self.data[0].energy;
  result[nresultValues - 2] := self.data[self.count - 1].energy;
  result[nresultValues - 1] := self.data[self.count - 1].energy;

  k := 2;
  for i := 1 to self.count - 2 do
  begin

    result[k] := self.data[i].energy;
    result[k + 1] := self.data[i].energy;

    k := k + 2;
  end;


end;

function Tdataloadset.GetPointCount: Byte;
var
  nValues: byte;
begin
  nValues := self.count + 1;
  result := nValues + (nValues - 2);
end;

function Tdataloadset.GetTimeValues: TListValues;
var
  i: integer;
  nValues: Integer;
  buferArr: array[0..23] of double;
  nresultValues: Integer;
begin

  nValues := self.count + 1;

  buferArr[0] := 0;
  buferArr[nValues - 1] := 1;

  for i := 1 to nValues - 2 do
  begin
    buferArr[i] := buferArr[i - 1] + self.data[i - 1].time;

  end;

  nresultValues := nValues + (nValues - 2);

  result[0] := 0;
  result[nresultValues - 1] := 1;

  for i := 0 to nValues - 3 do
  begin

    result[2 * i + 1] := buferArr[i + 1];

    result[2 * i + 2] := buferArr[i + 1];

  end;

end;

procedure Tdataloadset.SetEqualEnergyFractions;
var
  energyVolumeDefault: double;
  i: integer;
begin

  energyVolumeDefault :=1/Self.count;

  for i := 0 to self.count - 1 do
    self.data[i].energy := energyVolumeDefault/self.data[i].time;

end;

end.


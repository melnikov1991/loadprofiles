program Project2;

uses
  Vcl.Forms,
  MenuLoadProfiles in 'MenuLoadProfiles.pas' {Form3},
  MainMenu in 'MainMenu.pas' {Form1},
  —reateFormLoadProfile in '—reateFormLoadProfile.pas' {Form2},
  TimeIntervalFrame in 'TimeIntervalFrame.pas' {Frame2: TFrame},
  LoadProfile in 'LoadProfile.pas',
  DatabaseLoadProfile in 'DatabaseLoadProfile.pas',
  EnergyFractionFrame in 'EnergyFractionFrame.pas' {Frame3: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.

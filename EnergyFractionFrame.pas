unit EnergyFractionFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  VclTee.TeeGDIPlus, System.ImageList, Vcl.ImgList, VCLTee.TeEngine,
  VCLTee.Series, Vcl.StdCtrls, Vcl.ExtCtrls, VCLTee.TeeProcs, VCLTee.Chart,
  Vcl.Grids;

type
  TFrame3 = class(TFrame)
    TimeIntervalStringgrid: TStringGrid;
    Chart1: TChart;
    Button1: TButton;
    Series1: TLineSeries;
    ImageList1: TImageList;
    Series2: TLineSeries;
    CheckBox1: TCheckBox;
    procedure Chart1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Chart1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
    function NotFirstLastPoint(i: byte): boolean;
  public
    { Public declarations }
  end;

var
  DragIdx: integer = -1;

implementation

{$R *.dfm}

procedure TFrame3.Chart1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  i: Integer;
  xx: Integer;
  yy: Integer;
begin
//                             and NotFirstLastPoint(i)
  if Button = mbLeft then
  begin
    DragIdx := -1;
    for i := 0 to Series2.Count - 1 do
    begin
      xx := Series2.CalcXPos(i);
      yy := Series2.CalcYPos(i);
//        and ( (i=0) or (not (i mod 2 = 0)))
      if (Sqr(xx - X) + Sqr(yy - Y) <= 5 * 5) then
      begin
        DragIdx := i;
        Break;
      end;
    end;
  end



end;

procedure TFrame3.Chart1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  leftValue: Double;
  rightValue: Double;
  xx: Double;
  yy: Double;
  deltaYY: Double;
begin

  leftValue := Series2.XValues[DragIdx - 1];
  rightValue := Series2.XValues[DragIdx + 1];

  if (ssLeft in Shift) and (DragIdx >= 0) then
  begin

    Series2.GetCursorValues(xx, yy);

    if DragIdx=0 then DragIdx:=1;
    if DragIdx=series2.Count-1 then DragIdx:=series2.Count-2;


    if (not DragIdx mod 2 = 0) or (DragIdx=0) then
    begin
//      showmessage('������');

      deltaYY := Series2.YValues[DragIdx] - yy;

      Series2.YValues[DragIdx] := yy;
      Series2.YValues[DragIdx - 1] := yy;

      Series2.YValues[DragIdx + 1] := Series2.YValues[DragIdx + 1] + deltaYY;
      Series2.YValues[DragIdx + 2] := Series2.YValues[DragIdx + 2] + deltaYY;
    end
    else
    if (DragIdx mod 2 = 0) or (DragIdx=Series2.Count-1) then
    begin

//      showmessage('��������');
      deltaYY := Series2.YValues[DragIdx] - yy;

      Series2.YValues[DragIdx] := yy;
      Series2.YValues[DragIdx + 1] := yy;

      Series2.YValues[DragIdx  -1] := Series2.YValues[DragIdx -1] + deltaYY;
      Series2.YValues[DragIdx  -2] := Series2.YValues[DragIdx -2] + deltaYY;

    end;

    Chart1.Repaint;
  end;
  //

end;

procedure TFrame3.CheckBox1Click(Sender: TObject);
begin

if Series1.Marks.Visible and series2.Marks.Visible then
 begin
   Series1.Marks.Visible:=false;
   series2.Marks.Visible:=false;

 end
   else
  begin
   Series1.Marks.Visible:=true;
   series2.Marks.Visible:=true;
  end;



end;

function TFrame3.NotFirstLastPoint(i: byte): boolean;
begin

  result := true;

  if i in [Chart1.SeriesList[1].Count - 1, 0] then
    exit(false)
end;

end.


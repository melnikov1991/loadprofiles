unit TimeIntervalFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VclTee.TeeGDIPlus,
  Vcl.ComCtrls, Vcl.Grids, Vcl.StdCtrls, VclTee.TeEngine, Vcl.ExtCtrls,
  VclTee.TeeProcs, VclTee.Chart, VclTee.Series, math, System.ImageList,
  Vcl.ImgList;

type
  TFrame2 = class(TFrame)
    Chart1: TChart;
    TimeIntervalStringgrid: TStringGrid;
    Series1: TLineSeries;
    ImageList1: TImageList;
    Button1: TButton;
    ComboBox1: TComboBox;
//    procedure UpDown2Click(Sender: TObject; Button: TUDBtnType);
    procedure Chart1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Chart1MouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure ComboBox1Change(Sender: TObject);
  private
    procedure SetIntervalsLabels;
    procedure SetValueForAllRows(value: Double);
    procedure UpdateChart(value: Double);
    function NotFirstLastPoint(i: Integer): boolean;
    procedure UpdateTimeStringGrid;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DragIdx: Integer;

implementation

{$R *.dfm}



function TFrame2.NotFirstLastPoint(i: Integer): boolean;
begin

  result := true;

  if i in [Chart1.SeriesList[0].Count - 1, 0] then
    exit(false)

end;

procedure TFrame2.Chart1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  xx: Integer;
  yy: Integer;
  i: Integer;
begin

  if Button = mbLeft then
  begin
    DragIdx := -1;
    for i := 0 to Series1.Count - 1 do
    begin
      xx := Series1.CalcXPos(i);
      yy := Series1.CalcYPos(i);
      if (Sqr(xx - X) + Sqr(yy - Y) <= 5 * 5) and NotFirstLastPoint(i) then
      begin
        DragIdx := i;
        Break;
      end;
    end;
  end

end;

procedure TFrame2.Chart1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  xx: Double;
  yy: Double;
  leftValue: Double;
  rightValue: Double;
begin

  leftValue := Series1.XValues[DragIdx - 1];
  rightValue := Series1.XValues[DragIdx + 1];

  if (ssLeft in Shift) and (DragIdx >= 0) then
  begin

    Series1.GetCursorValues(xx, yy);

    if (xx > leftValue) and (xx < rightValue) then
      Series1.XValues[DragIdx] := xx;

    UpdateTimeStringGrid;

    Chart1.Repaint;
  end;
  //

end;

procedure TFrame2.ComboBox1Change(Sender: TObject);
var
  timeCount: Integer;
  value: double;
begin

  timeCount:=strtoint(ComboBox1.Text);

  TimeIntervalStringgrid.RowCount := timeCount+1;

  value := 1 / timeCount;

  SetIntervalsLabels;

  SetValueForAllRows(value);

  UpdateChart(value);


end;

procedure TFrame2.SetIntervalsLabels;
var
  i: Integer;
begin
  for i := 1 to TimeIntervalStringgrid.RowCount - 1 do
    TimeIntervalStringgrid.Cells[0, i] := '�������� ' + i.ToString;
end;

procedure TFrame2.SetValueForAllRows(value: Double);
var
  i: Integer;
begin
  for i := 1 to TimeIntervalStringgrid.RowCount - 1 do
    TimeIntervalStringgrid.Cells[1, i] := roundto(value, -4).ToString;
end;

procedure TFrame2.UpdateChart(value: Double);
var
  i: Integer;
begin

  Chart1.SeriesList[0].Clear;

  for i := 0 to strtoint(ComboBox1.Text) do
    Chart1.SeriesList[0].AddXY(i * value, 0);

end;

procedure TFrame2.UpdateTimeStringGrid;
var
  I: Integer;
  bufArray:array[0..23] of double;
begin

for I := 0 to chart1.SeriesList[0].Count-2 do
  bufArray[i]:=chart1.SeriesList[0].XValue[i+1]-chart1.SeriesList[0].XValue[i];

for I := 1 to TimeIntervalStringgrid.RowCount-1 do
    TimeIntervalStringgrid.Cells[1,i]:=floattostr(bufArray[i-1]);


end;

end.

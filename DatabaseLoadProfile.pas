unit DatabaseLoadProfile;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  NameLoadProfile, LoadProfile;

type
  TdataItem = record
    name: string[10];
    data: TLoadProfile;
  end;

  TDataBaseLoadProfile = record
    data: array[0..49] of TdataItem;
  end;

implementation

end.


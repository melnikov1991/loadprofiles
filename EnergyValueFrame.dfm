inherited Frame1: TFrame1
  inherited Chart1: TChart
    Title.Text.Strings = (
      #1069#1085#1077#1088#1075#1077#1090#1080#1095#1077#1089#1082#1080#1077' '#1080#1085#1090#1077#1088#1074#1072#1083#1099)
    object Series2: TLineSeries
      SeriesColor = clRed
      Brush.BackColor = clDefault
      LinePen.Width = 2
      Pointer.HorizSize = 6
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.VertSize = 6
      Pointer.Visible = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  inherited TimeIntervalStringgrid: TStringGrid
    Left = 567
    Top = 53
    Width = 214
    ExplicitLeft = 567
    ExplicitTop = 53
    ExplicitWidth = 214
  end
  inherited LabeledEdit2: TLabeledEdit
    Visible = False
  end
  inherited UpDown2: TUpDown
    Visible = False
  end
end
